# README #


## Setup of automated testing environment

## On Windows:

1. Install Node: [downloads for Node.js each platform](http://nodejs.org/download/);
1. Install Selenium webdriver: [download chrome driver](http://chromedriver.storage.googleapis.com/index.html);
1. Put in environment variables in your PATH;
1. Go to the location where you are going to create your test project and either open up a command line or terminal window there;
1. Run: “npm install selenium-webdriver” to install Selenium webdriver in the current project directory;
1. Pull tests from repo to the current folder;
1. Install mocha: `npm install –g mocha`;
1. Then, execute the code by running: `mocha name_of_test.js`.

## On Mac

1. Install Homebrew: `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`;
1. Install node: `brew install node`;
1. Install selenium web driver: `npm install --save selenium-webdriver`;
1. Make sure that you have installed JDK (java);
1. Install Mocha: `npm install --save mocha@2.0.1`;
1. Pull project with tests;
1. Enter that folder and then execute the code by running: `mocha name_of_test.js`.

